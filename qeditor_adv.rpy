#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2022  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#################################################################################
# Editor labels for developers
label qeditor_supernew:
    python:
        elemt = 0
        bosse = 0
        queid = len(allquests)
        waves = 3

        avail = []
        mobid = []
        subid = 0
        bosid = 0

    menu:
        "Please select the predominant element:"
        "Fire":
            $ elemt = Ele_Fire
        "Water":
            $ elemt = Ele_Water
        "Nature":
            $ elemt = Ele_Nature
        "Light":
            $ elemt = Ele_Light
        "Shadow":
            $ elemt = Ele_Shadow

    $ elemt_txt = ParseEle(elemt)
    menu:
        "Please select the BOSS element (current: [elemt_txt]):"
        "Fire":
            $ bosse = Ele_Fire
        "Water":
            $ bosse = Ele_Water
        "Nature":
            $ bosse = Ele_Nature
        "Light":
            $ bosse = Ele_Light
        "Shadow":
            $ bosse = Ele_Shadow

    $ queid = int(renpy.input("Please define the quest ID, default = [queid], special = 90000", allow="0123456789"))
    menu:
        "Please select the theme"
        "The Empire":
          python:
            avail=[950001, 950002, 950003, 950004, 950005]
            subid=renpy.random.choice(avail)
            avail.remove(subid)
            bosid=renpy.random.choice(avail)
            avail.remove(bosid)
            mobid=avail
        #"The Bandits":
        #"The Elves":
        #"The Dark Horde":
        #"The Darklanders":
        #"The Rebels":
        #"The Land of Fire":
        "The Wizard Order":
          python:
            avail=[950700, 950701, 950702]
            subid=renpy.random.choice(avail)
            avail.remove(subid)
            bosid=renpy.random.choice(avail)
            avail.remove(bosid)
            mobid=avail
        #"The Dwarves":
        "Custom":
          python:
            bosid=int(renpy.input("Please define boss ID", allow="0123456789"))
            subid=int(renpy.input("Please define extra ID", allow="0123456789"))
            avail=renpy.input("Please define mobs ID (comma-separated) (boss=[bosid], sub=[subid])", allow="0123456789,").split(",")
            mobid=[]
            for i in avail:
                mobid.append(int(i))

    # Generate the struct and save it
    # TODO: If renpy.exists() with 96 then upgrade else do nothing?
    python:
        qeid = int(queid)
        qefl = 1
        if qeid > 90000:
            qeid = queid - 90000
            qefl = 4

        # 2 then 3
        qdif = 2 + (qeid//2)*5 + (qeid%2)*3

        allquests.append({
        "quest_id": queid,
        "difficulty": qdif, # FIXME too steep
        "requeriment": qeid-1,
        "cost": 1+(qeid / 5),
        "flags": qefl,
        "music": "bgm03",
        "bg": "battle",
        "loot": [
                    ["1010", int(1000+(qeid/5*25))],
                    ["1020", int(100+(qeid/5*10))],
                    ["1030", int(10+(qeid/5))]
                ],
        "waves": []})

        # Generate waves
        #current=dl_search(allquests, "quest_id", queid)
        current=len(allquests)-1
        qedit=allquests[current]
        it = ifte(qeid % 5 == 0, 4, 3) # 4 waves every 5 fights
        i = 0
        while i < it:
            qedit["waves"].append([])
            j = 0
            k = ifte((i+1) == it, 1, ifte((i+1) == 2, 2, 3))
            while j < k:
                j+=1
                if k == 1:
                    tmp_sp = bosid + 10000 # FIXME
                elif k == 2:
                    tmp_sp = subid
                else:
                    tmp_sp = renpy.random.choice(mobid)
                if k == 1:
                    tmp_nm = "BOSS" # FIXME
                else:
                    tmp_nm = "Enemy %d" % j # FIXME
                qedit["waves"][i].append({
                "name": tmp_nm,
                "sprite": int(tmp_sp),
                "attribute": int(ifte((i+1) == it, bosse, elemt)),
                "boss": ifte((i+1) == it, True, False)
            })
            i += 1

        renpy.notify("New quest added: %d" % queid)

    #return
    $ qeditor_save()
    jump quest_editors
