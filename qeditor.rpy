#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#################################################################################
# Editor labels for developers
screen quest_editor():
    modal True
    frame:
        background Frame("gui/frame.png", 0, 0)
        xalign 0.5
        yalign 0.5
        ymargin 15
        ypadding 10
        xmargin 10

        hbox:
            spacing 30
            xmaximum 0.85
            ymaximum 0.85

            # Navigation
            viewport:
                #child_size (350, 1000)
                mousewheel True
                draggable True
                pagekeys True
                xfill False
                xmaximum 350
                scrollbars "vertical"

                vbox:
                    spacing 2
                    label _("Quest list")
                    $i=0
                    for entry in allquests:
                        textbutton "{size=18}"+_("%02d (D %d) (C %d)" % (entry["quest_id"], entry["difficulty"], entry["cost"]))+"{/size}" action SetVariable("current", i)
                        $i+=1
                    $del i

            # News screen
            viewport:
                #child_size (0.4, 0.8)
                mousewheel True
                #draggable True
                #arrowkeys True
                xfill False
                scrollbars "vertical"

                vbox:
                    xalign 0.5
                    spacing 20

                    $ qedit=allquests[current]
                    $ qreq=qedit["requeriment"]

                    hbox:
                      spacing 128
                      vbox:
                        label _("Quest %d" % (qedit["quest_id"]))
                        hbox:
                            spacing 10
                            textbutton _("-") action Function(qeditor, "quest_id", -1)
                            label ("%d" % qedit["quest_id"])
                            textbutton _("+") action Function(qeditor, "quest_id", 1)
                        null height 25


                      vbox:
                        ###########################################
                        label _("Requeriment")
                        hbox:
                            spacing 10
                            textbutton _("-") action Function(qeditor, "requeriment", -1)
                            label ("%d" % qedit["requeriment"])
                            textbutton _("+") action Function(qeditor, "requeriment", 1)
                        null height 15


                    ###########################################
                    label _("Difficulty")
                    hbox:
                        spacing 10
                        textbutton _("-10") action Function(qeditor, "difficulty", -10)
                        textbutton _("-5") action Function(qeditor, "difficulty", -5)
                        textbutton _("-1") action Function(qeditor, "difficulty", -1)
                        label _("%d" % qedit["difficulty"])
                        textbutton _("+1") action Function(qeditor, "difficulty", 1)
                        textbutton _("+5") action Function(qeditor, "difficulty", 5)
                        textbutton _("+10") action Function(qeditor, "difficulty", 10)
                    null height 15


                    hbox:
                      spacing 128
                      vbox:
                        ###########################################
                        label _("Cost")
                        hbox:
                            spacing 10
                            textbutton _("-") action Function(qeditor, "cost", -1)
                            label _("%d" % qedit["cost"])
                            textbutton _("+") action Function(qeditor, "cost", 1)
                        null height 15


                      vbox:
                        ###########################################
                        label _("Flags")
                        hbox:
                            spacing 10
                            textbutton _("-") action Function(qeditor, "flags", -1)
                            label _("%d" % qedit["flags"])
                            textbutton _("+") action Function(qeditor, "flags", 1)
                      text "None: 0\nClear Gems: 1\nTutorial Gems: 2\nSpecial: 4\nFirst Loot: 8\nDouble XP: 16\nDouble GP: 32" size 12
                    null height 15



                    ###########################################
                    label _("Waves")
                    vbox:
                        spacing 0
                        $ wc=0
                        null height 10
                        for wave in qedit["waves"]:
                            $wc+=1
                            label _("{size=22}Wave %d{/size}" % wc)
                            $ mc=0
                            for mob in wave:
                                $mc+=1
                                label _("{size=20}%s{/size}" % mob["name"])
                                textbutton "{size=20}%d{/size}" % mob["sprite"] action None
                                hbox:
                                    spacing 7
                                    textbutton "{size=20}Ele: %s{/size}" % ParseEle(mob["attribute"]) action Function(qeditor_input, "waves", wc-1, "element", key3=mc-1, key4="attribute")
                                    textbutton "{size=20}Boss: %s{/size}" % str(mob["boss"]) action Function(qeditor_input, "waves", wc-1, "bool", key3=mc-1, key4="boss")
                                    null width 20
                                    textbutton _("X Delete") action  Function(qeditor_delete, "waves", wc-1, mc-1)
                            null height 20
                            textbutton "{size=22}"+_("Add Monster")+"{/size}" action Function(qeditor_addmonster, wc)
                            textbutton "{size=22}"+_("Delete Wave")+"{/size}" action Function(qeditor_delete, "waves", wc-1)
                            null height 20
                        null height 20
                        textbutton _("Add Wave") action Function(qeditor_addwave)


                    ###########################################
                    label _("Loot")
                    vbox:
                        spacing 0
                        $i=0
                        for lt, ch in qedit["loot"]:
                            hbox:
                                spacing 1
                                $ input=Input(
                                    value=QEditorInputValue(lt, "loot", i, "str", key3=0),
                                    copypaste=True,
                                    allow="0123456789",
                                    length=9)
                                button:
                                    #key_events True
                                    action input.enable
                                    add input

                                label ": "

                                $ input=Input(
                                    value=QEditorInputValue(ch, "loot", i, "int", key3=1),
                                    copypaste=True,
                                    allow="0123456789",
                                    length=5)
                                button:
                                    #key_events True
                                    action input.enable
                                    add input

                                null width 40
                                textbutton _("X Delete") action None
                            $i+=1
                        textbutton _("Add Loot") action Function(qeditor_addloot)
                    null height 15

                    ###########################################
                    null height 40
                    hbox:
                        spacing 25

                        textbutton _("Save") action Function(qeditor_save)

                        textbutton _("Close") action Return()
                        textbutton _("New Quest") action Function(qeditor_new)
                        textbutton _("New S.") action Function(qeditor_new, 1)
                        textbutton _("Auto-New") action Jump("qeditor_supernew")

    ## Right-click and escape refresh screen
    key "game_menu" action Function(RestartInteraction)#Return()
    key "K_RETURN" action Function(RestartInteraction)#Return()

init python:
    def RestartInteraction():
        renpy.restart_interaction()
        return


    def qeditor(key, operation, override=False):
        global qedit
        qedit=allquests[current]
        print str(qedit["quest_id"])
        if override:
            qedit[key]=operation
        else:
            qedit[key]+=operation
        return

    def qeditor_save():
        global alltquests, allquests
        alltquests["Main"].sort(key=lambda x: x["quest_id"])
        allquests=alltquests["Main"]
        f=open(get_path("quests.editor.json"), "w")
        json.dump(allquests, f, indent=4, separators=(',', ': '))
        f.close()
        renpy.notify("File saved as quests.editor.json")
        return

    def qeditor_new(special=False):
        global allquests
        if not special:
            qeid=len(allquests)
            qefl=1
            qec=1
        else:
            qeid=90000
            qefl=4
            qec=20

        allquests.append({
        "quest_id": qeid,
        "difficulty": qeid*5,
        "requeriment": qeid-1,
        "cost": qec,
        "flags": qefl,
        "music": "bgm03",
        "bg": "battle",
        "loot": [
                    ["1010", 1000+(qeid//5*25)],
                    ["1020", 100+(qeid//5*10)],
                    ["1030", 10+(qeid//5)]
                ],
        "waves": []})
        renpy.notify("New quest added: %d" % qeid)
        return

    def qeditor_addloot():
        global qedit
        qedit=allquests[current]
        qedit["loot"].append(["1000", 0])
        renpy.notify("Added new loot field id 1000 (an invalid ID)")
        return

    def qeditor_addmonster(wave):
        global qedit
        qedit=allquests[current]
        pointer=qedit["waves"][wave-1]
        if len(pointer) >= 3:
            renpy.notify("Max monsters per wave reached!")
            return

        tmp_nm = renpy.invoke_in_new_context(renpy.input, "Monster Name?", default="New Monster")
        tmp_sp = renpy.invoke_in_new_context(renpy.input, "Monster Sprite?", default="950000")
        pointer.append({
            "name": tmp_nm,
            "sprite": int(tmp_sp),
            "attribute": 1,
            "boss": False
        })
        return

    def qeditor_addwave():
        global qedit
        qedit=allquests[current]
        qedit["waves"].append([])
        renpy.notify("Blank wave created\nThis is buggy, remember to add monsters")
        return


    def qeditor_delete(key1, key2=None, key3=None, key4=None):
        global qedit
        qedit=allquests[current]
        if key2 is None:
            del qedit[key1]
            #del allquests[current][key1]
        elif key3 is None:
            del qedit[key1][key2]
            #del allquests[current][key1][key2]
        elif key4 is None:
            del qedit[key1][key2][key3]
            #del allquests[current][key1][key2][key3]
        else:
            del qedit[key1][key2][key3][key4]
            #del allquests[current][key1][key2][key3][key4]
        return

    def qeditor_input(key1, key2=None, tp="int", temp="", key3=None, key4=None):
        #temp=renpy.input("Please insert new value for this variable.\nCurrent value: [variable]")
        global qedit
        qedit=allquests[current]
        variable=""

        # Key looping
        if key2 is None:
            target=qedit[key1]
        elif key3 is None:
            target=qedit[key1][key2]
        elif key4 is None:
            target=qedit[key1][key2][key3]
        else:
            target=qedit[key1][key2][key3][key4]

        # Convert input if needed
        # tp int → convert to int
        # tp key → Replace the key
        # tp bool → Toogles the value
        if tp == "int":
            try:
                variable=int(temp)
            except:
                renpy.notify("Invalid numeric input")
                variable=0 # Sorry, but gets bad otherwise
        elif tp == "key":
            try:
                qedit[key1][temp]=copy.copy(target)
                renpy.notify("Key replaced")
                del qedit[key1][key2] # Not sure "del target" would work
                return
            except:
                renpy.notify("ERROR, doing nothing")
                variable=target
        elif tp == "bool":
            try:
                variable=(not target)
                print "New boolean: %s" % str(variable)
            except:
                print "Illegal boolean"
                renpy.notify("Illegal boolean")
                variable=target
        elif tp == "element":
            try:
                variable=ifte(target >= 5, 0, target+1)
                print "Value switched: %s" % str(variable)
            except:
                traceback.print_exc()
                print "Elemental error"
                renpy.notify("Elemental error")
                variable=target
        else:
            try:
                variable=str(temp)
            except:
                renpy.notify("Invalid string input")

        # Save input
        # Key looping
        if key2 is None:
            target=qedit[key1]=variable
        elif key3 is None:
            target=qedit[key1][key2]=variable
        elif key4 is None:
            target=qedit[key1][key2][key3]=variable
        else:
            target=qedit[key1][key2][key3][key4]=variable

        return

    class QEditorInputValue(InputValue):
        def __init__(self, variable, key1, key2, inpu="int", key3=None, key4=None):
            try:
                self.variable = variable
            except:
                self.variable = ""
            self.inpu = inpu

            self.default=True
            self.editable=True

            self.key1=key1
            self.key2=key2
            self.key3=key3
            self.key4=key4

        def get_text(self):
            try:
                return str(self.variable)
            except:
                return ""
            #return globals()[self.variable]

        def set_text(self, s):
            #globals()[self.variable] = s
            qeditor_input(self.key1, self.key2, self.inpu, s, key3=self.key3, key4=self.key4)

        def enter(self):
            renpy.restart_interaction()
            #renpy.run(self.Disable())
            #raise renpy.IgnoreEvent()

label quest_editors:
    $ qedit=allquests[0]
    $ current=0
    $print("")
    $print(".:: THE BUILT-IN QUEST EDITOR ::.")
    $print("Use ESC to redraw screen, saving input")
    $print("")
    call screen quest_editor
    $print("Quest Editor closed")
    menu:
        "Save Changes":
            $ qeditor_save()
        "Discard Changes":
            pass
    jump restore

