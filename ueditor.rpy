########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Editor labels for developers
screen units_editor():
    modal True
    frame:
        background Frame("gui/frame.png", 0, 0)
        xalign 0.5
        yalign 0.5
        ymargin 15
        ypadding 10
        xmargin 10

        hbox:
            spacing 30
            xmaximum 0.85
            ymaximum 0.85

            # Navigation
            viewport:
                #child_size (350, 1000)
                mousewheel True
                draggable True
                pagekeys True
                xfill False
                xmaximum 350
                scrollbars "vertical"

                vbox:
                    spacing 2
                    label _("Unit list")
                    $i=0
                    for entry in allunitsbase:
                        textbutton "{size=18}"+_("%d - %s" % (entry["unit_id"], entry["name"]))+"{/size}" action SetVariable("current", i)
                        $i+=1
                    $del i

            # News screen
            viewport:
                #child_size (0.4, 0.8)
                mousewheel True
                #draggable True
                #arrowkeys True
                xfill False
                scrollbars "vertical"

                vbox:
                    xalign 0.5
                    spacing 20

                    $ uedit=allunitsbase[current]

                    hbox:
                        label _("%s %s" % (star_write(uedit["rare"]), uedit["name"]))
                        null width 30
                        textbutton "%s" % ifte(uedit["sex"], "(F)", "(M)"):
                            action Function(ueditor, "sex", ifte(uedit["sex"],
                                            0, 1), True)
                    hbox:
                        spacing 10
                        label _("ID: ")
                        $ input=Input(
                            value=UEditorInputValue(uedit, "unit_id", None, "int", dp=True),
                            copypaste=True,
                            allow="0123456789",
                            length=8)
                        button:
                            #key_events True
                            action input.enable
                            add input
                        null width 180
                        label _("(%d)" % uedit["unit_base_id"]):
                            text_size 24
                        null width 24
                        label _("MaxLv. %d" % uedit["max_level"]):
                            text_size 24
                    hbox:
                        spacing 10
                        label _("Name: ")
                        $ input=Input(
                            value=UEditorInputValue(uedit, "name", None, "str", dp=True),
                            copypaste=True,
                            allow="qwertyuiopasdfghjklçzxcvbnm QWERTYUIOPASDFGHJKLÇZXCVBNM1234567890-+=!(),.:;",
                            length=32)
                        button:
                            #key_events True
                            action input.enable
                            add input
                    hbox:
                        textbutton _("-") action Function(ueditor, "rare", -1)
                        label ("Rarity: %s" % star_write(uedit["rare"])):
                            text_size 24
                        textbutton _("+") action Function(ueditor, "rare", 1)

                    null height 25
                    label _("Flavor text:")
                    #label _(uedit["flavor"])
                    $ input=Input(
                        value=UEditorInputValue(uedit, "flavor", None, "str", dp=True),
                        copypaste=True,
                            #allow="qwertyuiopasdfghjklçzxcvbnm QWERTYUIOPASDFGHJKLÇZXCVBNM1234567890-+=!()",
                        length=512)
                    button:
                        #key_events True
                        action input.enable
                        add input
                    null height 25

                    hbox:
                        spacing 24
                        label _("HP %d" % uedit["hp"])
                        label _("ATK %d" % uedit["strength"])
                    null height 25

                    hbox:
                        textbutton _("-") action Function(ueditor, "attribute", -1)
                        label ("Element: %s" % ParseEle(uedit["attribute"]))
                        textbutton _("+") action Function(ueditor, "attribute", 1)
                    hbox:
                        textbutton _("-") action Function(ueditor, "job", -1)
                        label ("Job: %s" % ParseJob(uedit["job"]))
                        textbutton _("+") action Function(ueditor, "job", 1)
                    null height 25
                    hbox:
                        textbutton _("-") action Function(ueditor, "flags", -1)
                        label _("Flags %d" % uedit["flags"])
                        textbutton _("+") action Function(ueditor, "flags", 1)
                    text "None: 0\nCannot levelup: 1\nCannot party: 2\nEXP+: 4\nEvoMat: 8\nDouble GP: 16\nUnsellable: 32\nSuper EvoMat: 64" size 12
                    null height 25

                    # TODO: Open skills.json and parse the IDs
                    label _(".:: Skills ::.")
                    hbox:
                        label _("Active ID: "):
                            text_size 20
                        $ input=Input(
                            value=UEditorInputValue(uedit, "skill_id", None, "int", dp=True),
                            copypaste=False,
                            allow="0123456789",
                            length=6,
                            size=20)
                        button:
                            #key_events True
                            action input.enable
                            add input

                    hbox:
                        label _("Passive ID: "):
                            text_size 20
                        $ input=Input(
                            value=UEditorInputValue(uedit, "ability_id", None, "int", dp=True),
                            copypaste=False,
                            allow="0123456789",
                            length=6,
                            size=20)
                        button:
                            #key_events True
                            action input.enable
                            add input

                    # TODO: Skills, portraits
                    ###########################################
                    null height 40
                    hbox:
                        spacing 25

                        textbutton _("Save") action Function(ueditor_save)

                        textbutton _("Close") action Return()
                        null width 20
                        textbutton _("Recalc") action Function(ueditor_recalc)
                        null width 20
                        textbutton _("New Sub") action Function(ueditor_new, True)
                        textbutton _("New Unit") action Function(ueditor_new)

    ## Right-click and escape refresh screen
    key "game_menu" action Function(RestartInteraction)#Return()
    key "K_RETURN" action Function(RestartInteraction)#Return()

init python:
    def ParseJob(job):
        if job == 1:
            return "Swordsman"
        elif job == 2:
            return "Unused"
        elif job == 3:
            return "Mage"
        elif job == 4:
            return "Archer"
        elif job == 5:
            return "Gunner"
        elif job == 6:
            return "Special"
        else:
            return "ERROR (%d)" % job

    def RestartInteraction():
        renpy.restart_interaction()
        return


    def ueditor(key, operation, override=False):
        global uedit
        uedit=allunitsbase[current]
        print str(uedit["unit_id"])
        if override:
            uedit[key]=operation
        else:
            uedit[key]+=operation
        return

    def ueditor_save():
        global allunitsbase
        allunitsbase.sort(key=lambda x: x["unit_id"])
        f=open(get_path("units.editor.json"), "w")
        json.dump(allunitsbase, f, indent=4, separators=(',', ': '))
        f.close()
        renpy.notify("File saved as units.editor.json")
        return

    def ueditor_new(sub=False):
        global allunitsbase
        if not sub:
            ueid=(100000+len(allunitsbase))*100
            rar=1
            nam="New unit"
            flav="A standard unit in the game."
            attr=1
            sex=0
            job=1
        else:
            ueid=allunitsbase[current]["unit_id"]/100*100+1 # Truncate
            rar=allunitsbase[current]["rare"]+1
            nam=allunitsbase[current]["name"]+" +"+str(rar)
            attr=allunitsbase[current]["attribute"]
            sex=allunitsbase[current]["sex"]
            job=allunitsbase[current]["job"]
            flav=allunitsbase[current]["flavor"]

        allunitsbase.append({"skill_id": 0,
  "strength": 500,
  "rare": rar,
  "name": nam,
  "ability_id": 0,
  "attribute": attr,
  "hp": 500,
  "unit_id": ueid,
  "sex": sex,
  "max_level": 10+(rar*10),
  "job": job,
  "cost": rar**2,
  "flags": 0,
  "unit_base_id": ueid/100,
  "flavor": flav})
        renpy.notify("New unit added: %d" % ueid)
        return

    def ueditor_recalc():
        global uedit
        uedit=allunitsbase[current]
        job=uedit["job"]
        rar=uedit["rare"]
        STR=0
        HP=0
        # Sum must be 800
        if job == 1:
            STR=300;HP=500; # Swordsman (DEF+)
        elif job == 2:
            STR=400;HP=400; # Assassin (TODO)
        elif job == 3:
            STR=500;HP=300; # Mage (ATK+)
        elif job == 4:
            STR=400;HP=400; # Archer (balanced)
        elif job == 5:
            STR=600;HP=200; # Gunner (unused)
        elif job == 6:
            STR = 0;HP = 0; # Special
        # Apply rarity modified
        STR*=(rar+1)/2.0
        HP*=(rar+1)/2.0
        STR=max(int(STR), 1)
        HP=max(int(HP), 1)
        # Save data
        renpy.notify("%s\nHP %d->%d \n ATK %d->%d" % (uedit["name"],
                     uedit["hp"], HP,
                     uedit["strength"], STR))
        uedit["strength"]=copy.copy(STR)
        uedit["hp"]=copy.copy(HP)
        allunitsbase[current]["strength"]=copy.copy(STR)
        allunitsbase[current]["hp"]=copy.copy(HP)
        # Correct unit family and max level if needed
        allunitsbase[current]["unit_base_id"]=allunitsbase[current]["unit_id"]/100
        allunitsbase[current]["max_level"]=10+(rar*10)
        return


    def ueditor_delete(key1, key2=None, key3=None, key4=None):
        global uedit
        uedit=allunitsbase[current]
        if key2 is None:
            del uedit[key1]
            #del allunitsbase[current][key1]
        elif key3 is None:
            del uedit[key1][key2]
            #del allunitsbase[current][key1][key2]
        elif key4 is None:
            del uedit[key1][key2][key3]
            #del allunitsbase[current][key1][key2][key3]
        else:
            del uedit[key1][key2][key3][key4]
            #del allunitsbase[current][key1][key2][key3][key4]
        return

    def ueditor_input(key1, key2=None, tp="int", temp="", key3=None, key4=None):
        #temp=renpy.input("Please insert new value for this variable.\nCurrent value: [variable]")
        global uedit
        uedit=allunitsbase[current]
        variable=""

        # Key looping
        if key2 is None:
            target=uedit[key1]
        elif key3 is None:
            target=uedit[key1][key2]
        elif key4 is None:
            target=uedit[key1][key2][key3]
        else:
            target=uedit[key1][key2][key3][key4]

        # Convert input if needed
        # tp int → convert to int
        # tp key → Replace the key
        # tp bool → Toogles the value
        if tp == "int":
            try:
                variable=int(temp)
            except:
                renpy.notify("Invalid numeric input")
                variable=0 # Sorry, but gets bad otherwise
        elif tp == "key":
            try:
                uedit[key1][temp]=copy.copy(target)
                renpy.notify("Key replaced")
                del uedit[key1][key2] # Not sure "del target" would work
                return
            except:
                renpy.notify("ERROR, doing nothing")
                variable=target
        elif tp == "bool":
            try:
                variable=(not target)
                print "New boolean: %s" % str(variable)
            except:
                print "Illegal boolean"
                renpy.notify("Illegal boolean")
                variable=target
        else:
            try:
                variable=str(temp)
            except:
                renpy.notify("Invalid string input")

        # Save input
        # Key looping
        if key2 is None:
            target=uedit[key1]=variable
        elif key3 is None:
            target=uedit[key1][key2]=variable
        elif key4 is None:
            target=uedit[key1][key2][key3]=variable
        else:
            target=uedit[key1][key2][key3][key4]=variable

        return


    class UEditorInputValue(InputValue):
        def __init__(self, variable, key1, key2, inpu="int", key3=None, key4=None, dp=False):
            try:
                self.variable = variable
            except:
                self.variable = ""
            self.inpu = inpu
            self.short_display = dp

            self.default=True
            self.editable=True

            self.key1=key1
            self.key2=key2
            self.key3=key3
            self.key4=key4

        def get_text(self):
            try:
                if not self.short_display:
                    return str(self.variable)
                else:
                    return str(self.variable[self.key1])
            except:
                traceback.print_exc()
                return ""
            #return globals()[self.variable]

        def set_text(self, s):
            #globals()[self.variable] = s
            ueditor_input(self.key1, self.key2, self.inpu, s, key3=self.key3, key4=self.key4)

        def enter(self):
            renpy.restart_interaction()
            #renpy.run(self.Disable())
            #raise renpy.IgnoreEvent()

label units_editors:
    $ uedit=allunitsbase[0]
    $ current=0
    $print("")
    $print(".:: THE BUILT-IN UNIT EDITOR ::.")
    $print("Use ESC to redraw screen, saving input")
    $print("")
    call screen units_editor
    $print("Unit Editor closed")
    menu:
        "Save Changes":
            $ ueditor_save()
        "Discard Changes":
            pass
    jump restore

