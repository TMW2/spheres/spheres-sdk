########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################

init -3 python:
    import json, copy, traceback
    config.autoreload = False
    config.save_on_mobile_background = False
    ERR_JSONDECODER=101
    ERR_INVALID=103
    if (persistent.allfiles is None):
        persistent.allfiles=[]
    allfiles=[]

    # Elements
    Ele_Fire    =1
    Ele_Water   =2
    Ele_Nature  =3
    Ele_Light   =4
    Ele_Shadow  =5

    def ParseEle(ele):
        if ele == 0:
            return "Neutral"
        elif ele == 1:
            return "Fire"
        elif ele == 2:
            return "Water"
        elif ele == 3:
            return "Nature"
        elif ele == 4:
            return "Light"
        elif ele == 5:
            return "Shadow"
        else:
            return "ERROR (%d)" % ele

    # Smart Print command
    def stdout(message):
        print(message)
        renpy.write_log("[DEBUG] %s" % message)
        return

    # IF Then Else (IFTE)
    def ifte(ifs, then, elses):
        if (ifs):
            return then
        else:
            return elses

    # Global classes
    # We need to override standard list method. Original by Triptych (stackoverflow)
    class dlist(list):

        def __setitem__(self, index, value):
            size = len(self)
            if index >= size:
                self.extend(None for _ in range(size, index + 1))

            list.__setitem__(self, index, value)

    class ExecuteOnCall():
        def __init__(self, callable, *args, **kwargs):
            self.callable = callable
            self.args = args
            self.kwargs = kwargs

        def __call__(self):
            rv = self.callable(*self.args, **self.kwargs)
            return rv

        def id(self):
            return self.__call__()

    class RetString():
        def __init__(self, string):
            self.string = string

        def __call__(self):
            return self.string

        def id(self):
            return self.__call__()

    class ExtraImage(renpy.display.im.Image):
        """
        Custom image manipulator, based on bink's code, topic 11732
        """

        def __init__(self, loc, **properties):
            """
            @param loc: Where the image really is (get_path already applied)
            """

            super(ExtraImage, self).__init__(loc, **properties)
            self.loc = loc

        def load(self, unscaled=False): # W0221
            try:
                #page = open(self.loc, "rb")
                #pd = page.read()
                #picdata = os.tmpfile()
                #picdata.write(pd)
                #picdata.seek(0) # reset seek position
                stdout("Requested to open: %s" % repr(self.loc))
                picdata = open(self.loc, "rb")
                stdout("Picdata is open (%s)" % self.loc)

                if unscaled:
                    surf = renpy.display.pgrender.load_image_unscaled(picdata, self.loc)
                else:
                    surf = renpy.display.pgrender.load_image(picdata, self.loc)

                stdout("Picdata was closed")
                picdata.close()
                #page.close()

                return surf
            except Exception:
                if renpy.config.missing_image_callback:
                    im = renpy.config.missing_image_callback(self.loc)
                    if im is None:
                        raise
                    return im.load()
                raise

    def virtpos(posix):
        if isinstance(posix, float):
            return posix*1024
        else:
            return posix/1024.0

    # File Managment Functions
    def get_path(path):
        return renpy.loader.get_path(path)

    def get_path_if_exists(path):
        return renpy.loader.transfn(path)

    stdout("======================= %s %s" % (config.name, config.version))
    print "[STDBY] Loading Basic functions......."

    # Search for array[?][key]==search in an array of dicts
    # Returns the dictionary, or returns ERR_INVALID
    def dl_search(array, key, search):
        try:
            r=(item for item in array if item[key] == search).next()
        except:
            r=ERR_INVALID
        if r is None:
            r=ERR_INVALID
            stdout("dlsearch: r is None")
        return r

    def GAME_LOADER():
        global allunitsbase, allunits, allquests, allstory, allworld, alltaverns
        global alltquests, tr_uptodate, tr_memcheck

        # FIXME: Error handling
        # Load unit data
        #allunitsbase=json.loads(requests.get("http://"+HOST+'/units.json').text)
        f=open(get_path_if_exists("units.json"), "r")
        allunitsbase=json.load(f)
        f.close()

        # Reorder unit data
        allunits={}
        for j in allunitsbase:
            allunits[j["unit_id"]]=j
        allunitsbase.sort(key=lambda x: x["unit_id"])

        # Load summons data
        f=open(get_path_if_exists("summons.json"), "r")
        allsummons=json.load(f)
        f.close()

        # Load quest data
        f=open(get_path_if_exists("quests.json"), "r")
        alltquests=json.load(f)
        f.close()
        alltquests["Main"].sort(key=lambda x: x["quest_id"])
        allquests=alltquests["Main"]

        # Load story data
        f=open(get_path_if_exists("story.json"), "r")
        allstory=json.load(f)
        f.close()

        # Load world data
        f=open(get_path_if_exists("world.json"), "r")
        allworld=json.load(f)
        f.close()

        # Load tavern data
        f=open(get_path_if_exists("bar.json"), "r")
        alltaverns=json.load(f)
        f.close()

        stdout("[OK] Game data loaded to memory")
        tr_memcheck=True
        return True

    def card_composite(cid, path):
        # We need to try to get rarity
        try:
            r=allunits[int(cid)]["rare"]
        except:
            r=4 # FIXME

        # We need to try to get the element
        try:
            e=allunits[int(cid)]["attribute"]
            print str(e)
        except:
            e=0

        return Composite(
            (640, 960),
            (0, 0), "gfx/cards/bg.png",
            (0, 0), path,
            (0, 0), "gfx/cards/"+str(r)+".png",
            (0, 0), "gfx/cards/ele/"+str(e)+".png")
        # size,)

    # TODO: square_ and gfx/square/units
    # Converts regular image filepaths to displayables names and vice-versa
    def img_regex(name, reverse=False):
        if not reverse:
            # Extension is ommited, add them yourself!
            if name.startswith("unit_"):
                return "gfx/units/%s" % name.replace("unit_")
            elif name.startswith("mob_"):
                return "gfx/mobs/%s" % name.replace("mob_")
            elif name.startswith("dialog_"):
                return "gfx/dialog/%s" % name.replace("dialog_")
            elif name.startswith("bg "):
                return "gfx/bg/%s" % name.replace("bg ")
            elif name.startswith("summon_"):
                return "gfx/summons/%s" % name.replace("summon_")
            else:
                print("ERROR: Not a regular filename")
                return name
        else:
            if name.startswith("gfx/units/"):
                return "unit_%s" % name
            elif name.startswith("gfx/mobs/"):
                return "mob_%s" % name
            elif name.startswith("gfx/dialog/"):
                return "dialog_%s" % name
            elif name.startswith("gfx/bg/"):
                return "bg %s" % name
            elif name.startswith("gfx/summons/"):
                return "summon_%s" % name
            else:
                print("ERROR: Not a regular display name")
                return name

    # Loads a sound called VAL
    def get_sfx(val, ext):
        # Search for the sound
        show_img(val, False, ext=ext)

        valb=dl_search(persistent.allfiles, 0, val)[1]
        if valb == ERR_INVALID:
            print ("Invalid Sound: %s (%s)" % (path, val))
            return "sfx/regnum.mp3" # FIXME
        return valb

    # Load sprite images: "unit"
    for file in renpy.list_files():
        fn=file.replace('gfx/units/','').replace('/', ' ').replace('.png','')
        if file.startswith('gfx/units/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "unit_"+fn
                #renpy.image(name, Image(file, yanchor=1.0))
                renpy.image(name, card_composite(fn, file))

                dl=dl_search(persistent.allfiles, 0, name)
                if dl is not ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue

    # Load sprite images: "mob"
    for file in renpy.list_files():
        fn=file.replace('gfx/mobs/','').replace('/', ' ').replace('.png','')
        if file.startswith('gfx/mobs/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "mob_"+fn
                renpy.image(name, Image(file, yanchor=1.0))
                if not name in persistent.allfiles:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue


    # Load sprite images: "dialog"
    for file in renpy.list_files():
        fn=file.replace('gfx/dialog/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/dialog/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "dialog_"+fn
                renpy.image(name, Image(file, yanchor=1.0))
                dl=dl_search(persistent.allfiles, 0, name)
                if dl is not ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue


    # Load background images: "bg"
    for file in renpy.list_files():
        fn=file.replace('gfx/bg/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/bg/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "bg "+fn
                renpy.image(name, Frame(file, 0, 0))
                dl=dl_search(persistent.allfiles, 0, name)
                if dl is not ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue


    # Load summon images: "summon"
    for file in renpy.list_files():
        fn=file.replace('gfx/summons/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/summons/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "summon_"+fn
                renpy.image(name, Image(file, yanchor=1.0))
                dl=dl_search(persistent.allfiles, 0, name)
                if dl is not ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue

    def star_write(am):
        i, st = 0, ""
        while i < am:
            i+=1
            st+="★"
        return st


    # Overrides renpy.image() method
    def new_img(name, where):
        # d: Downloaded path
        if not isinstance(name, tuple):
            name = tuple(name.split())

        #d = renpy.renpy.easy.displayable(where)
        if True or renpy.android:
            d=ExtraImage(where)
        else:
            d = renpy.renpy.easy.displayable(where)
        renpy.renpy.display.image.register_image(name, d)
        return

    # Retrieves Ren'Py displayable name associated to PATH
    def get_img(path):
        # Search for the image name
        val=img_regex(path, True)
        show_img(val, False)

        valb=dl_search(persistent.allfiles, 0, val)[1]
        if valb == ERR_INVALID:
            print ("Invalid Image: %s (%s)" % (path, val))
            return "gfx/spinner.png"
        return valb

    # Overrides renpy.show() and renpy.image() methods
    # Missing: transient=False, munge_name=True
    def show_img(img, show=True, at_list=[ ], tag=None, zorder=None, behind=[ ], atl=None, what=None, layer=None, ext=".webp"):
        global tr_loading
        # Image exists, display it
        if img in allfiles:
            if show:
                renpy.show(img, at_list=at_list, tag=tag, zorder=zorder, behind=behind, atl=atl, what=what, layer=layer)
            return

        # Have we downloaded this image previously?
        path=dl_search(persistent.allfiles, 0, img)
        print str(path)

        # Image doesn't exists, we must download it
        while (path == ERR_INVALID):
            tr_loading=True
            # Latest version converts these formats to WebP
            if ext in [".png", ".jpg", ".jpeg"]:
                ext=".webp"
            # Otherwise, preserve extension.
            if True or renpy.android:
                addr="extra_%s%s" % (img.replace(" ", "_"), ext)
            else:
                addr="extra/%s%s" % (img.replace(" ", "_"), ext)

            f=open(get_path(addr), "w")
            stdout("Downloading additional file: %s" % img.replace(" ", "_"))
            try:
                x=requests.get("%s://%s/assets/%s?token=%s" % (ifte(persistent.ssl_enabled, "https", "http"), HOST, img.replace(" ", "_"), get_token()), verify=False) # , timeout=8.0 → Need to handle sudden death
            except:
                # Oh noes - something went *terribly* wrong
                traceback.print_exc()
                x=requests.Response()
                x.status_code=403

            if x.status_code == 200:
                f.write(x.content)
                f.close()
                # Android needs paths to be saved by full
                # But audio hates it, so.
                if ext not in [".mp3", ".ogg"]:
                    addr=get_path(addr)
                path=((img, addr))
                persistent.allfiles.append(path)
            else:
                stdout("ERROR FOR: %s://%s/assets/%s?token=%s" % (ifte(persistent.ssl_enabled, "https", "http"), HOST, img.replace(" ", "_"), get_token()))
                try:
                    retry=renpy.call_screen("confirm", "Error downloading file.\nError Code: %d\n\nRetry?" % x.status_code, Return(True), Return(False))
                    if not retry:
                        if tag is None:
                            tag=img
                        path=None
                        if show:
                            if tag is None:
                                tag=img
                            print("tag is %s" % str(tag))
                            renpy.show("spinner", at_list=at_list, tag=tag, zorder=zorder, behind=behind, atl=atl, what=what, layer=layer) # TODO Show error
                        tr_loading=False
                        return
                    # TODO: “Retry?”
                except:
                    print("Failed, trying again")

        # Image exists, but wasn't loaded yet
        if (path != ERR_INVALID and path is not None):
            print "Detected not loaded image"
            # Valid Image Extensions: PNG, JPG, JPEG, GIF, WEBP
            if ext in [".png", ".jpg", ".jpeg", ".gif", ".webp"]:
                # Maybe it is an unit
                if img.startswith("unit_"):
                    new_img(img, card_composite(img.replace("unit_", ""), path[1]))
                else:
                    new_img(img, path[1])

            stdout("registered image: "+path[1])
            allfiles.append(img)
            if show:
                renpy.show(img, at_list=at_list, tag=tag, zorder=zorder, behind=behind, atl=atl, what=what, layer=layer)
            tr_loading=False
            return

        # Something went wrong
        stdout("show_img reached abnormal ending")
        return





